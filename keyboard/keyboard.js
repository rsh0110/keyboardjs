const Keyboard = {
    elements:{
        main:null,
        keysContainer:null,
        keys:[]
    },

    eventsHandlers:{
        output:null,
        onclose:null
    },
    
    properties:{
        value:"",
        capslock:false
    },

    init(){
        //Create the main elements
    
        this.elements.main = document.createElement("div");
        this.elements.keysContainer = document.createElement("div");
3
        //Setup main elements
        this.elements.main.classList.add("keyboard","keyboard--hidden");
        this.elements.keysContainer.classList.add("keyboard__keys");
        this.elements.keysContainer.appendChild(this._createKeys());
        
        this.elements.keys = this.elements.keysContainer.querySelectorAll(".keyboard__key");


        //Add to Dom
        this.elements.main.appendChild(this.elements.keysContainer);
        document.body.appendChild(this.elements.main);
        
        document.querySelectorAll(".use-keyboard-input").forEach( elements => {
            elements.addEventListener("focus" ,() =>{
                this.open(elements.value, currentValue =>{
                    elements.value = currentValue;
                 });
            });
    
        });

    },

    _createKeys(){
        const fragment = document.createDocumentFragment();
        const keyLayout=[
            "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "backspace",
            "q", "w", "e", "r", "t", "y", "u", "i", "o", "p",
            "caps", "a", "s", "d", "f", "g", "h", "j", "k", "l", "enter",
            "done", "z", "x", "c", "v", "b", "n", "m", ",", ".", "?",
            "space"
        ];
        //Create HTML for an icon

        const createIconHtml = (icon_name) =>{
            return `<i class="material-icons">${icon_name}</i>`;
            
        };

        keyLayout.forEach(key=>{
            const keyElement = document.createElement("button");
            const insretLineBreak = ["backspace", "p", "enter", "?"].indexOf(key)!== -1;

            //Add attributes/classes
            keyElement.setAttribute("type", "button");
            keyElement.classList.add("keyboard__key");

            switch(key){
                case "backspace":
                    keyElement.classList.add("keyboard__key--wide");
                    keyElement.innerHTML = createIconHtml("backspace");
                    
                    keyElement.addEventListener('click',()=>{
                        this.properties.value = this.properties.value.substring(0,this.properties.value.length-1);
                        this._trigerEvent("oninput");
                    }) ;
                    break;

                case "caps":
                    keyElement.classList.add("keyboard__key--wide" ,"keyboard__key--activatable");
                    keyElement.innerHTML = createIconHtml("keyboard_capslock");
                    
                    keyElement.addEventListener('click',()=>{
                        this._toggleCapsLock();
                        keyElement.classList.toggle("keyboard__key--active", this.properties.capslock);
                    }) ;
                    break;

                case "enter":
                    keyElement.classList.add("keyboard__key--wide");
                    keyElement.innerHTML = createIconHtml("keyboard_return");
                    
                    keyElement.addEventListener('click',()=>{
                        this.properties.value += "\n";
                        this._trigerEvent("oninput");
                    }) ;
                    break;

                case "space":
                    keyElement.classList.add("keyboard__key--extrawide");
                    keyElement.innerHTML = createIconHtml("space_bar");
                    
                    keyElement.addEventListener('click',()=>{
                        this.properties.value += " ";
                        this._trigerEvent("oninput");
                    }) ;
                    break;

                case "done":
                    keyElement.classList.add("keyboard__key--wide","keyboard__key--dark");
                    keyElement.innerHTML = createIconHtml("check_circle");

                    keyElement.addEventListener('click',()=>{
                        this.close();
                        this._trigerEvent("oninput");
                    }) ;
                    break;
                
                default:
                    keyElement.textContent = key.toLowerCase();
                
                    keyElement.addEventListener('click',()=>{
                        this.properties.value += this.properties.capslock ? key.toUpperCase(): key.toLowerCase(); 
                        this._trigerEvent("oninput")
                    });
                    break;
                    
            }
            
            fragment.appendChild(keyElement);

            if(insretLineBreak){
                fragment.appendChild(document.createElement("br"));
            }

        });

        return fragment;
    },

    _trigerEvent(handlerName){
        if (typeof this.eventsHandlers[handlerName]=='function'){
            this.eventsHandlers[handlerName](this.properties.value);
        }
    },

    _toggleCapsLock(){
        this.properties.capslock = !this.properties.capslock;
        for(const key of this.elements.keys){
            if(key.childElementCount===0){
                key.textContent = this.properties.capslock ? key.textContent.toUpperCase() : key.textContent.toLowerCase();
            }  
        }
    },

    open(initialValue, oninput, onclose){
        this.properties.value = initialValue || "";
        this.eventsHandlers.oninput = oninput;
        this.eventsHandlers.onclose = onclose;
        this.elements.main.classList.remove("keyboard--hidden");
        console.log("opened")
    },

    close(){
        this.properties.value = "";
        this.eventsHandlers.oninput = oninput;
        this.eventsHandlers.onclose = onclose;
        this.elements.main.classList.add("keyboard--hidden");
    }
};

window.addEventListener("DOMContentLoaded", function(){
    Keyboard.init();
    

});